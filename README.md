# Desafio Mobile Trainee

Parabéns você acaba de ser aprovado para uma vaga de **trainee** em uma importante Fábrica de Softwares.

Como primeira missão pra mostrar serviço pro **Gerente de Projetos** você recebe a tarefa de desenvolver parte de um aplicativo que seu time está construindo.

O sistema é para uma empresa de streaming de filmes que se chama **CineFlix**.

-----------------------

## As especificações da API são:

### 1. Padrão de Código

Todo o código deverá ser feito utilizando o padrão **Clean Code** e **em inglês**, caso contrário o projeto não será avaliado.

>  Sugestão: Assistir com atenção a seguinte playlist: https://www.youtube.com/playlist?list=PLMdYygf53DP5Sc6yFYs6ZmjsuuA2fu0TK

-----------------------

### 2. Padrão de API

Utilizar o padrão de API REST

>  Atenção: Domine os conceitos desse vídeo => https://www.youtube.com/watch?v=7Cbd8WBGrq4

Caso tenha alguma dúvida, faça novas pesquisas sobre o tema.

-----------------------

### 3. Telas

Desenvolver as seguintes telas:

#### 3.1 Tela Home

A tela principal do aplicativo deve exibir a mensagem de boas vindas vinda do endpoint "/".

#### 3.2 Tela Filmes

Esta tela deve listar todos os filmes do endpoint "/movies"

  - Deve ser adicionado um filtro que permita listar os filmes por categorias o endpoint que faz esta ação é `/movies?categoriId=id_categoria`

Dica: Use um botão de filtro, padrão dropdown, como na imagem em anexo. Que tenha como item padrão "Todos" e os demais itens sejam obtidos do endpoint "categories"

#### 3.3 Tela Diretores

Esta tela deve listar todos os diretores do endpoint `/directors`

- Tanto a tela Filmes e a tela Diretores, devem permitir que seja selecionado um objeto da lista e seja direcinado para uma página de **Detalhes daquele objeto**

A documentação da API backend está no seguinte endereço: https://cine-flix.herokuapp.com/api/

> Dica: Utilize o programa [Postman](https://www.postman.com/) para testar os endpoints
>
> Crie uma coleção pra cada um dos recursos
>
> Crie uma requisição para um dos endpoint de cada recurso

-----------------------

### 4. Framework

O aplicativo deve ser construído utilizando o Framework [Flutter](https://flutter.dev/)

---

### 5. Biblioteca de Estilos

O aplicativo `Flutter` deve utilizar como base a biblioteca [Cupertino](https://flutter.dev/docs/development/ui/widgets/cupertino) 

-----------------------

### 6. Repositório

Deve ser criado um repositório público no [Gitlab](https://gitlab.com) com o nome: `desafio-cine-mobile`.

-----------------------

### 7. Padrão de Commits

O seu código deve ser comitado utilizando o [Padrão Git Flow](https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow).

Atenção: Pesquise bem o que é `Git Flow` antes de subir os commits!

> Dica: Use o `shell ZSH` como padrão do seu sistema e habilite nele o plugin `git-flow-avh`

-----------------------

### 8. Dúvidas

Qualquer dúvida referente a documentação do backend deve ser tirada unicamente com o Mestre Jedi construtor da API: https://t.me/dcyrillo3

-----------------------

### 9. Suporte?

Para o frontend do seu aplicativo seus únicos e exclusivos amigos serão os mecanismos de busca e fóruns da web. Sendo mais claro, você não poderá pedir ajuda para NENHUM outro desenvolvedor. Nesta fase, é esperado que o desenvolvedor seja capaz de resolver este nível de especificação por conta própria, e em caso de dúvidas, ou problemas, saiba buscar as respostas na internet, em sites como o [Stackoverflow](https://stackoverflow.com/).

---

### 10. Revisão

Revise com atenção se todas as exigências técnicas foram atingidas

---

### 11. Entrega

Ao final do seu trabalho envie um e-mail para `talentos@haizen.tech` com o assunto `DESAFIO: Mobile trainee`

📬️ **O e-mail deve conter:**

- O link para o repositório no `Gitlab` com seu projeto
- O link de um vídeo gravado da tela do celular, mostrando o aplicativo e todos os recursos que desenvolveu

-----------------------

Agora que já recebeu todas as informações é hora de mostrar pro seu **Gerente de Projetos** que você tem competência para estar na empresa.

Boa sorte jovem Padawan e mão na massa!